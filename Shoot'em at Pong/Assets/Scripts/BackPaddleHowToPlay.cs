﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackPaddleHowToPlay : MonoBehaviour {
    
    float m_speed = 2.0f;
    
    float m_ranRange = 6.0f;

    Vector3 m_targetPos = Vector3.zero;

    AudioSource m_sound;

    // Use this for initialization
    void Start ()
    {
        m_targetPos = Vector3.up * Random.Range(-m_ranRange, m_ranRange);

        m_sound = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if ((transform.position - m_targetPos).magnitude < 0.5f)
        {
            m_targetPos = Vector3.up * Random.Range(-m_ranRange, m_ranRange);
        }

        m_targetPos.x = transform.position.x;
        m_targetPos.z = transform.position.z;

        transform.position = Vector3.Lerp(transform.position, m_targetPos, m_speed * Time.deltaTime);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_sound.Stop();
        BulletHowToPlay other = collision.gameObject.GetComponent<BulletHowToPlay>();

        if (other)
        {
            Vector3 newDirection = collision.transform.position - transform.position;

            if (Mathf.Abs(newDirection.y) < 0.15f)
            {
                newDirection.y = 0;
            }
            else
            {
                if (Mathf.Abs(newDirection.y) < 0.3f)
                {
                    newDirection.y /= 3.0f;
                }
            }


            newDirection.Normalize();

            other.m_direction = newDirection + (-Vector3.left / 4.0f );

            other.m_direction.Normalize();

            m_sound.Play();
        }
    }

}
