﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float m_speed = 5.0f;

    public float m_randomYDirectionRange = 0.3f;

    public Vector3 m_direction = Vector3.right;

    public bool m_hasReflected = false;

	// Use this for initialization
	void Start () {

        m_direction.y += Random.Range(-m_randomYDirectionRange, m_randomYDirectionRange);

        m_direction.Normalize();
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Mathf.Abs(m_direction.x) < 0.2f)
        {
            m_direction.x *= 1.5f;

            m_direction.Normalize();
        }

        transform.position = transform.position + (m_direction * m_speed * Time.deltaTime);

        if (transform.position.x > 7.0f || transform.position.x < -8.0f)
        {
            if (transform.position.x > 7.0f)
            {
                GameController.Score(1);
            }

            Destroy(gameObject);
        }

        if (transform.position.y >= 5.0f || transform.position.y < -5.0f)
        {
            if(Mathf.Abs(transform.position.y + m_direction.y) > 5.0f)
                m_direction.y *= -1.0f;
        }

    }
}
