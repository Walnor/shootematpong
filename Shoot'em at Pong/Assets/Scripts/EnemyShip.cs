﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour {

    int m_Life = 1;

    [SerializeField] GameObject m_bullet;

    [SerializeField] GameObject p_FireRate;
    [SerializeField] GameObject p_BulletSpeed;
    [SerializeField] GameObject p_MultiShot;
    [SerializeField] GameObject p_ExtraLife;


    public Transform m_maxBounds;
    public Transform m_minBounds;

    public float m_FireRate = 0.5f;
    public float m_speed = 1.0f;

    float timer = 0.0f;

    [SerializeField] GameObject m_hitParticles;

    Vector3 destination = Vector3.zero;

    // Use this for initialization
    void Start ()
    {
        destination = new Vector3(Random.Range(m_minBounds.position.x, m_maxBounds.position.x), Random.Range(m_minBounds.position.y, m_maxBounds.position.y), 0);
    }
	
	// Update is called once per frame
	void Update ()
    {

        Movement();

        timer += Time.deltaTime;

        if (timer >= m_FireRate)
        {
            timer = 0.0f;

            Fire();
        }

    }

    void Movement()
    {
        if ((transform.position - destination).magnitude < 0.5f)
        {
            destination = new Vector3(Random.Range(m_minBounds.position.x, m_maxBounds.position.x), Random.Range(m_minBounds.position.y, m_maxBounds.position.y), 0);
        }

        Vector3 newPos = Vector3.MoveTowards(transform.position, destination, m_speed * Time.deltaTime);
        transform.position = newPos;
    }

    void Fire()
    {
        if (!m_bullet)
            return;

        GameObject bullet = Instantiate(m_bullet);

        Bullet b = bullet.GetComponent<Bullet>();

        b.m_direction *= -1.0f;
        b.m_hasReflected = true;

        bullet.transform.position = transform.position + (Vector3.left * 0.4f);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Bullet bullet = collision.gameObject.GetComponent<Bullet>();

        if (bullet)
        {
            Destroy(bullet.gameObject);

            m_Life--;

            GameObject particles = Instantiate(m_hitParticles);
            particles.transform.position = transform.position;

            if (m_Life < 0)
            {
                GameController.Score(5);

                for (int i = 0; i < 3; i++)
                {
                    Fire();
                }

                if (Random.Range(0, 100) < 30)
                {
                    SpawnPowerUp();
                }

                Destroy(gameObject);
            }
        }
    }

    private void SpawnPowerUp()
    {
        float R = Random.Range(0.0f, 100.0f);

        print("PowerUp");

        if (R < 20.0f)
        {
            GameObject BS = Instantiate(p_BulletSpeed);
            BS.transform.position = transform.position;
            return;
        }
        if (R < 60.0f)
        {
            GameObject FR = Instantiate(p_FireRate);
            FR.transform.position = transform.position;
            return;
        }
        if (R < 90.0f)
        {
            GameObject MS = Instantiate(p_MultiShot);
            MS.transform.position = transform.position;
            return;
        }
        if (R < 100.0f)
        {
            GameObject MS = Instantiate(p_ExtraLife);
            MS.transform.position = transform.position;
            return;
        }
    }
}
