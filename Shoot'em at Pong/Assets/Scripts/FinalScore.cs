﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalScore : MonoBehaviour {

    int m_FinalScore = 0;

	// Use this for initialization
	void Start ()
    {
        DontDestroyOnLoad(this);		
	}

    public int GiveScore()
    {
        return m_FinalScore;
    }

    public void GiveScore(int score)
    {
        m_FinalScore = score;
    }
}
