﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    static GameObject m_score;

    static GameObject m_EShip;

    static Transform m_enemyBoundsMax;
    static Transform m_enemyBoundsMin;

    [SerializeField] GameObject SK;

    [SerializeField] GameObject m_EnemyShip;

    [SerializeField] Transform m_maxBounds;
    [SerializeField] Transform m_minBounds;

    GameObject m_Player;

    private void Start()
    {
        m_score = SK;
        m_enemyBoundsMax = m_maxBounds;
        m_enemyBoundsMin = m_minBounds;
        m_EShip = m_EnemyShip;

        m_Player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if(!m_Player)        {
            int test = GameObject.FindGameObjectsWithTag("Bullet").Length;
            if (test == 0)
            {
                GameObject.FindGameObjectWithTag("Finish").GetComponent<FinalScore>().GiveScore(m_score.GetComponent<ScoreKeeper>().m_CurrentScore);
                SceneManager.LoadScene("High Scores", LoadSceneMode.Single);
            }
        }
    }

    static public void Score(int points)
    {
        if (!m_score)
            return;

        ScoreKeeper keeper = m_score.GetComponent<ScoreKeeper>();

        if (keeper)
        {
            keeper.MakeScore(points);

            if (keeper)
            {
                if (keeper.m_CurrentScore % 5 == 0)
                {
                    SpawnEnemyShip();
                }
            }
        }
    }

    static public void SpawnEnemyShip()
    {
        if (GameObject.FindGameObjectWithTag("Player"))
        {

            GameObject obj = Instantiate(m_EShip);

            obj.transform.position = new Vector3(Random.Range(m_enemyBoundsMin.position.x, m_enemyBoundsMax.position.x), Random.Range(m_enemyBoundsMin.position.y, m_enemyBoundsMax.position.y), 0);

            Vector3 newPos = obj.transform.position;

            if (newPos.y >= 0.0f)
            {
                newPos.y = 7.0f;
            }
            else
            {
                newPos.y = -7.0f;
            }

            obj.transform.position = newPos;

            EnemyShip enemyShip = obj.GetComponent<EnemyShip>();

            if (enemyShip)
            {
                enemyShip.m_maxBounds = m_enemyBoundsMax;
                enemyShip.m_minBounds = m_enemyBoundsMin;
            }
        }
    }
}
