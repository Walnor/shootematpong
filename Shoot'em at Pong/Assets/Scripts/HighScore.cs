﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour {

    int m_PlayerScore;

    string[] m_Users;
    int[] m_Scores;

    [SerializeField] Text t_PlayerScore;

    [SerializeField] Text[] t_TextScores;
    [SerializeField] Text[] t_TextPlace;

    // Use this for initialization
    void Start ()
    {
        FinalScore fs = GameObject.FindGameObjectWithTag("Finish").GetComponent<FinalScore>();
        m_PlayerScore = fs.GiveScore();

        Destroy(fs.gameObject);

        t_PlayerScore.text = "Your Score:      " + m_PlayerScore;

        ReadFile();

        for (int i = 0; i < m_Users.Length; i++)
        {
            t_TextScores[i].text = m_Scores[i].ToString();
            t_TextPlace[i].text = m_Users[i];
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void WriteFile()
    {
        string path = "Assets/Resources/test.txt";
        
        StreamWriter writer = new StreamWriter(path, false);
        writer.WriteLine("Test");
        for(int i = 0; i < 10; i++)
        {
            writer.WriteLine(m_Users[i] + " " + m_Scores[i]);
        }
        writer.Close();
    }


    void ReadFile()
    {
        string path = "Assets/Resources/HighScores.txt";
        
        StreamReader reader = new StreamReader(path);

        string[] fileString = reader.ReadToEnd().Split('\n');

        List<string> theList = new List<string>();

        foreach (string s in fileString)
        {
            theList.Add(s);
        }

        reader.Close();

        //m_Users = new string[fileString.Length];
        m_Scores = new int[fileString.Length];

        for (int i = 0; i < fileString.Length; i++)
        {
            string[] theScore = fileString[i].Split(' ');

            //m_Users[i] = theScore[0];
            m_Scores[i] = int.Parse(theScore[1]);
        }

        int playerScoreIndex = 11;

        for (int i = 9; i >= 0; i--)
        {
            if (m_Scores[i] < m_PlayerScore)
            {
                playerScoreIndex = i;
            }
        }

        theList.Insert(playerScoreIndex, "PLY " + m_PlayerScore);

        m_Users = new string[fileString.Length];
        m_Scores = new int[fileString.Length];

        for (int i = 0; i < 10; i++)
        {
            string[] theScore = theList[i].Split(' ');

            m_Users[i] = theScore[0];
            m_Scores[i] = int.Parse(theScore[1]);
        }
    }
}
