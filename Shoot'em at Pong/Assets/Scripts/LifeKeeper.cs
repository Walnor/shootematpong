﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeKeeper : MonoBehaviour {

    Player m_Player;

    Text m_Text;

	// Use this for initialization
	void Start () {
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        m_Text = gameObject.GetComponent<Text>();

    }
	
	// Update is called once per frame
	void Update () {
        m_Text.text = "Life: " + m_Player.m_Life.ToString();

        if (m_Player.m_Life < 0)
        {
            Destroy(gameObject);
        }
	}
}
