﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuEvents : MonoBehaviour
{
    public void PlayButton()
    {
        SceneManager.LoadScene("GameScreen", LoadSceneMode.Single);
    }
    public void HowToPlayButton()
    {
        SceneManager.LoadScene("HowToPlay", LoadSceneMode.Single);
    }
    public void MainMenuButton()
    {
        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }
}
