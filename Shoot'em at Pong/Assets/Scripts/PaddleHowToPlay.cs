﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleHowToPlay : MonoBehaviour {

    GameObject target = null;

    public float m_speed = 8.0f;

    float m_RandomTargetAngle;

    AudioSource m_sound;

    // Use this for initialization
    void Start ()
    {
        m_sound = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (target == null)
        {
            GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");

            target = GetClosestBullet(bullets);

            m_RandomTargetAngle = Random.Range(-0.4f, 0.4f);
        }

        if (target)
        {
            Vector3 Targetpos = target.transform.position;

            Targetpos.x = transform.position.x;
            Targetpos.z = transform.position.z;

            Targetpos.y += m_RandomTargetAngle;

            Vector3 newPos = Vector3.MoveTowards(transform.position, Targetpos , Time.deltaTime * m_speed);

            newPos.x = transform.position.x;
            newPos.z = transform.position.z;

            transform.position = newPos;
            
        }

		
	}

    private void LateUpdate()
    {

        if (target)
        {
            if (target.GetComponent<BulletHowToPlay>().m_direction.x < 0 || target.transform.position.x > transform.position.x)
                target = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_sound.Stop();
        BulletHowToPlay other = collision.gameObject.GetComponent<BulletHowToPlay>();        

        if (other)
        {
            Vector3 newDirection = collision.transform.position - transform.position;

            if (Mathf.Abs(newDirection.y) < 0.15f)
            {
                newDirection.y = 0;
            }
            else
            {
                if (Mathf.Abs(newDirection.y) < 0.3f)
                {
                    newDirection.y /= 3.0f;
                }
            }

            other.m_hasReflected = true;


            newDirection.Normalize();

            other.m_direction = newDirection + (Vector3.left / 4.0f );

            other.m_direction.Normalize();

            other.m_speed *= 1.5f;

            m_sound.Play();
        }
    }


    GameObject GetClosestBullet(GameObject[] bullets)
    {
        GameObject bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;

        foreach (GameObject potentialTarget in bullets)
        {
            currentPosition.y = potentialTarget.transform.position.y;

            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                BulletHowToPlay b = potentialTarget.gameObject.GetComponent<BulletHowToPlay>();
                if (b.transform.position.x < transform.position.x)
                {
                    if (b)
                    {
                        if (b.m_direction.x > 0.0f)
                        {
                            closestDistanceSqr = dSqrToTarget;
                            bestTarget = potentialTarget;
                        }
                    }
                }
            }
        }

        return bestTarget;
    }

}
