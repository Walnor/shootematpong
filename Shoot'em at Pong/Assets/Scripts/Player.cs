﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField] float m_speed = 5.0f;

    [SerializeField] Transform m_maxBounds;
    [SerializeField] Transform m_minBounds;

    [SerializeField] GameObject m_bullet;

    public int m_Life = 3;

    public float m_FireRate = 0.5f;

    float timer = 0.0f;

    int m_multishot = 1;
    float m_BulletSpeed = 2.0f;

    [SerializeField] GameObject m_hitParticles;
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () {

        Movement();

        timer += Time.deltaTime;

        if (timer >= m_FireRate)
        {
            timer = 0.0f;

            Fire();
        }

    }

    void Movement()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;

        Vector3 newPos = Vector3.Lerp(transform.position, mousePos, m_speed * Time.deltaTime);

        if (m_maxBounds && m_minBounds)
        {
            newPos.x = Mathf.Clamp(newPos.x, m_maxBounds.position.x, m_minBounds.position.x);
            newPos.y = Mathf.Clamp(newPos.y, m_maxBounds.position.y, m_minBounds.position.y);
        }

        transform.position = newPos;
    }

    void Fire()
    {
        if (!m_bullet)
            return;

        for (int i = 0; i < m_multishot; i++)
        {
            GameObject bullet = Instantiate(m_bullet);

            bullet.transform.position = transform.position + (Vector3.right * 0.4f);

            Bullet b = bullet.GetComponent<Bullet>();
            b.m_speed = m_BulletSpeed;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Bullet bullet = collision.gameObject.GetComponent<Bullet>();

        if (bullet)
        {
            if (bullet.m_hasReflected)
            {
                Destroy(bullet.gameObject);

                m_Life--;

                GameObject particles = Instantiate(m_hitParticles);
                particles.transform.position = transform.position;

                if (m_Life < 0)
                {
                    Destroy(gameObject);
                }
            }
            return;
        }

        PowerUp powerup = collision.gameObject.GetComponent<PowerUp>();

        if (powerup)
        {
            if (powerup.m_type == PowerUp.PowerUpType.BulletSpeed)
            {
                m_BulletSpeed *= 1.5f;
            }
            else
            if (powerup.m_type == PowerUp.PowerUpType.FireRate)
            {
                m_FireRate *= 0.9f;
            }
            else
            if (powerup.m_type == PowerUp.PowerUpType.Multishot)
            {
                m_multishot++;
            }
            else
            if (powerup.m_type == PowerUp.PowerUpType.ExtraLife)
            {
                m_Life++;
            }

            Destroy(collision.gameObject);
        }
    }
}
