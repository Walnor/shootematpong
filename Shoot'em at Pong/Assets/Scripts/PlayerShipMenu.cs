﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipMenu : MonoBehaviour {
    
    [SerializeField] GameObject m_bullet;
    
    public float m_FireRate = 0.5f;

    float timer = 0.0f;

    int m_multishot = 1;
    float m_BulletSpeed = 4.0f;

    [SerializeField] GameObject m_hitParticles;
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () {

        timer += Time.deltaTime;

        if (timer >= m_FireRate)
        {
            timer = 0.0f;

            Fire();
        }

    }

    void Fire()
    {
        if (!m_bullet)
            return;

        for (int i = 0; i < m_multishot; i++)
        {
            GameObject bullet = Instantiate(m_bullet);

            bullet.transform.position = transform.position + (Vector3.right * 0.4f);

            Bullet b = bullet.GetComponent<Bullet>();
            b.m_speed = m_BulletSpeed;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Bullet bullet = collision.gameObject.GetComponent<Bullet>();

        if (bullet)
        {
            if (bullet.m_hasReflected)
            {
                Destroy(bullet.gameObject);

                GameObject particles = Instantiate(m_hitParticles);
                particles.transform.position = transform.position;
            }
            return;
        }        
    }
}
