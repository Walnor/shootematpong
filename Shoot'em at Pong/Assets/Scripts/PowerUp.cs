﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    public float m_speed = 3.0f;

    public enum PowerUpType
    {
        BulletSpeed, FireRate, Multishot, ExtraLife
    }

    public PowerUpType m_type = PowerUpType.BulletSpeed;

    GameObject m_target;

	// Use this for initialization
	void Start ()
    {
        m_target = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (m_target)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_target.transform.position, Time.deltaTime * m_speed);
        }
        else
        {
            m_target = GameObject.FindGameObjectWithTag("Player");

            if (!m_target)
                Destroy(gameObject);
        }
		
	}
}
