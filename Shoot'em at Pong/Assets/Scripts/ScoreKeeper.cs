﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

    public Text m_textScore;

    public int m_CurrentScore = 0;

    public void MakeScore(int Points)
    {
        m_CurrentScore += Points;

        m_textScore.text = m_CurrentScore.ToString("00");
    }
}
